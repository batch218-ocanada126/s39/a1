const mongoose = require("mongoose");
const Course = require("../models/course.js");

/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	
	return newCourse.save().then((newCourse, error) => {
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}
*/
 

 // S39 ACTIVITY
 // Create a Single Course
 	module.exports.addCourse = (data) => {
 		console.log(data.isAdmin)

 		if(data.isAdmin) {
 			let newCourse = new Course({
 				name: data.course.name,
 				description: data.course.description,
 				price: data.course.price
 			});

 			return newCourse.save().then((newCourse, error) => {
 				if(error){
 					return error
 				}

 				return newCourse 
 			})
 		};

 		// If the user is not admin, then return this message as a promise to avoid errors
 		let message = Promise.resolve('User must be ADMIN to access this.')

 		return message.then((value) => {
 			return {value}
 		})
 	};
 // Create a Single Course END


module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
			return result;
	})
}

// GET specific course
module.exports.getCourse = (courseId) => {

						//inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
}
										// it will contain multiple fields
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Course.findByIdAndUpdate(courseId , 
			{			//req.body  
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}



	// my answer for s39
/*module.exports.addingCourse = (reqBody) => {

	let addCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	
	return addCourse.save().then((addCourse, error) => {
		if(error){
			return error;
		}
		else{
			return addCourse;
		}
	})
}*/


//s40

/*module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Course.findByIdAndUpdate(courseId , 
			{			//req.body  
				isActive: req.body.isActive
			}
		).then((archivedCourse, error) => {
			if(error){
				return false
			}
			
			return true
		})
	
}*/


// s40 ACTIVITY
module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archivedCourse, error) =>{
		if(error){
			return false
		}

		return {
			message : "Course archived successfully!"
		}
	});
}
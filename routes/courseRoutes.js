const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of a package
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


// S39 ACTIVITY
// Create a Single Course
// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

const data = {
	course: req.body,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// Create a Single Course END



// Get all courses
router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})


// Get all ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

// Get single course
router.get("/:courseId", (req, res) => {
							// retrieves the id from the url
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})


// Updating a single course
// lclhst4000/crs001/update - valid
// lclhst4000/update/crs001 - error
router.patch("/:courseId/update", auth.verify, (req,res) => 
{
	const newData = {
		course: req.body, 	//req.headers.authorization contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})


/*router.post("/addCourse", auth.verify, (req,res) =>{

	const addCourse = {
		course: req.body, 	//req.headers.authorization contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addingCourse(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})*/

//s40

// S40 ACTIVITY
// Archiving a single course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	});
});


 // for using ADMIN
/*router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const newData = {
		isActive: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(req.params.courseId, newData).then(result => {
		res.send(result)
	})
})*/

module.exports = router;
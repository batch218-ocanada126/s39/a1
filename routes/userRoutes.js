// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//S38 Actvity - possible answers

// A
router.post("/details", (req, res) => {
	userController.getProfileA(req.body).then(resultFromController => res.send(resultFromController));
})

// B
router.get("/details/:id", (req, res) => {
	userController.getProfileB(req.params.id).then(resultFromController => res.send(resultFromController))
});


// s40 ACTIVITY
// Archiving a single course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	});
});





//s41
// enroll user

router.post("/enroll", auth.verify, (req,res) => {

	let data ={
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id
		
	}

	userController.enroll(data).then(resultFromController =>{
		res.send(resultFromController)
	});
});



module.exports = router;